from random import randint, choice


class Mechanic(object):
    field = []
    zero_point = []

    def __init__(self, height, width):
        self.height = height
        self.width = width
        self.create_field()
        self.start()

    def start(self):
        self.list_of_zero()
        self.point()
        self.list_of_zero()
        self.point()

    def create_field(self):
        for x in range(self.width):
            self.field.append([0] * self.height)

    def point(self):
        x = randint(0, len(self.zero_point) - 1)
        self.field[self.zero_point[x][0]][self.zero_point[x][1]] = choice([2, 4])

    def print_field(self):
        for row in self.field:
            print "".join(str(row))
        print " "

    def list_of_zero(self):
        self.zero_point = []
        for i in range(len(self.field)):
            for j in range(len(self.field)):
                if self.field[i][j] == 0:
                    self.zero_point.append([i, j])

